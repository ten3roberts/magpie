/*
 * Copyright (c) 2021 Tim Roberts

 * Author: Tim Roberts

 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of
 * the Software, and to permit persons to whom the Software is furnished to do
 so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef MAGPIE_H
#define MAGPIE_H
#include <stddef.h>
#include <stdio.h>

typedef unsigned int mp_line_t;

enum MagpieStatus {
  MAGPIE_OK = 0,
  MAGPIE_INFO,
  MAGPIE_BAD_ALLOC,
  // Magpie does not know about this pointer
  // Either coming from external source, or not allocated at all
  MAGPIE_UNTRACKED,
  // Buffer overwrite
  MAGPIE_BUFFEROW,
};

// Prints:
// Total allocations
// Allocations remaining to be freed
// Returns number of remaining blocks
size_t magpie_status();

// Frees internal resources and terminates magpie
// Returns number of remaining blocks
// Use this in the end of the program
size_t magpie_terminate();

// Returns the total number of allocations
size_t magpie_total_allocs();

// Returns number of current allocations
size_t magpie_current_allocs();

// Returns sum of total allocations memory (constant time)
size_t magpie_current_size();

// Validates and returns status of pointer
// Checks for untracked/already freed and buffer overwrite
// Return MAGPIE_OK on success
// Should not be used instead of good memory management,
// but rather as way to validate pointers during debug
enum MagpieStatus magpie_validate(void *p);

// Validates all allocated pointers
// Slow and should thus not be used except in a debugger like gdb
void magpie_validate_all();

// Prints the n most frequent locations (file:line) of allocations
// -1 indicates to print all
void magpie_print_locations(long n);

// Sets the callback to use for printing
// Arg 1: status/level
// Arg2: Message string
// Default callback prints to stderr
// Status levels:
//   MAGPIE_INFO
//   MAGPIE_BAD_FREE
//   MAGPIE_BAD_ALLOC
//   MAGPIE_LEAK
void magpie_set_msgcallback(void (*callback)(enum MagpieStatus, char *));

// Adds a pointer to magpie tracking
// Replaces already tracked pointer cleanly
void magpie_track(void *p, size_t size, void *oldptr, size_t oldsize,
                  char padlen, const char *file, mp_line_t line);
// Removes a pointer from magpie tracking
enum MagpieStatus magpie_untrack(void *p, const char *file, mp_line_t line);

// Tracked malloc
void *magpie_malloc(size_t size, const char *file, mp_line_t line);
// Tracked calloc
void *magpie_calloc(size_t num, size_t elemsize, const char *file,
                    mp_line_t line);
// Tracked realloc
// Does not free p on failure
void *magpie_realloc(void *p, size_t size, const char *file, mp_line_t line);
// Tracked free, always use with magpie_malloc
void magpie_free(void *p, const char *file, mp_line_t line);

#ifndef MAGPIE_FAILURE
#define MAGPIE_FAILURE magpie_failure
#endif

#ifndef MAGPIE_INITIAL_CAPACITY
#define MAGPIE_INITIAL_CAPACITY 4
#endif
#ifndef MAGPIE_MSGSIZE
#define MAGPIE_MSGSIZE 512
#endif

#ifndef MAGPIE_PAD_PATTERN
#define MAGPIE_PAD_PATTERN "¤%@^`"
#endif

#define MAGPIE_PADLEN sizeof(MAGPIE_PAD_PATTERN)

#ifndef MAGPIE_INITIAL_LOCATION_CAPACITY
// Initial capacity for tracking locations of mallocs
// Smaller since it refers to unqiue file and lines for locations
#define MAGPIE_INITIAL_LOCATION_CAPACITY 16
#endif

#endif

#ifdef MAGPIE_IMPLEMENTATION

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void magpie_default_callback(enum MagpieStatus status, char *buf) {
  (void)status;
  fputs(buf, stderr);
}

static void (*magpie_msg_callback)(enum MagpieStatus,
                                   char *) = magpie_default_callback;

void magpie_set_msgcallback(void (*callback)(enum MagpieStatus, char *)) {
  magpie_msg_callback = callback;
}

static void magpie_print(int status, const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);

  char buf[MAGPIE_MSGSIZE];
  vsnprintf(buf, sizeof buf, fmt, args);

  magpie_msg_callback(status, buf);

  va_end(args);
}

struct MagpieAllocation {
  void *ptr;

  // For realloc
  size_t oldsize;
  void *oldptr;

  size_t size;
  const char *file;
  mp_line_t line;
  size_t num;
  // Padding at end to check for buffer ow
  char padlen;
};

typedef unsigned int (*MagpieHashFunc)(const void *);
// Returns 0 on equality
typedef int (*MagpieCmpFunc)(const void *, const void *);

// From https://gist.github.com/badboy/6267743
unsigned int magpie_allocation_hash(const void *p) {
  const struct MagpieAllocation *allocation = p;
  return ((size_t)allocation->ptr >> 4) + ((size_t)allocation->ptr >> 32);
}

static int magpie_allocation_cmp(const void *a, const void *b) {
  const struct MagpieAllocation *alloc_a = a;
  const struct MagpieAllocation *alloc_b = b;

  return alloc_a->ptr != alloc_b->ptr;
}

static void magpie_allocation_print(void *p) {
  struct MagpieAllocation *alloc = p;
  if (alloc == NULL) {
    magpie_print(MAGPIE_INFO, "  Allocation: (invalid)\n");
    return;
  }

  if (alloc->oldsize) {
    magpie_print(
        MAGPIE_INFO,
        "  Reallocated Addr: %p -> %p,\t size: %zu -> %zu, \t%s:%u #%u\n",
        alloc->oldptr, alloc->ptr, alloc->oldsize, alloc->size, alloc->file,
        alloc->line, alloc->num);
  } else {
    magpie_print(MAGPIE_INFO, "\tAddr: %p,\t size: %zu, \t%s:%u #%u\n",
                 alloc->ptr, alloc->size, alloc->file, alloc->line, alloc->num);
  }
}

typedef struct MagpieHashTable {
  // Distance between each element
  size_t stride;
  MagpieHashFunc hash_func;
  MagpieCmpFunc cmp_func;
  // Allocated size of array
  size_t capacity;

  // Number of entries in table
  size_t count;

  void *elements;
  char *occupied;
} MagpieHashTable;

// Creates a new hashtable
// `capacity` must be a power of 2
static MagpieHashTable magpie_ht_create(size_t stride, size_t capacity,
                                        MagpieHashFunc hash_func,
                                        MagpieCmpFunc cmp_func) {
  return (MagpieHashTable){
      .stride = stride,
      .hash_func = hash_func,
      .cmp_func = cmp_func,
      .capacity = capacity,
      .elements = malloc(stride * capacity),
      .occupied = calloc(capacity, 1),
      .count = 0,
  };
}

static void magpie_ht_destroy(MagpieHashTable *table) {
  free(table->elements);
  table->elements = NULL;

  free(table->occupied);
  table->occupied = NULL;

  table->count = 0;

  table->capacity = 0;
}

static void *magpie_ht_index(MagpieHashTable *table, size_t index) {
  return (char *)table->elements + index * table->stride;
}

static void magpie_ht_insert(MagpieHashTable *table, void *item);

// Resizes and rehashes
static void magpie_ht_resize(MagpieHashTable *table, size_t newcap) {
  void *old_elems = table->elements;
  char *old_occupied = table->occupied;
  size_t old_cap = table->capacity;

  table->capacity = newcap;
  table->count = 0;
  table->elements = malloc(table->capacity * table->stride);
  table->occupied = calloc(1, table->capacity);

  for (size_t i = 0; i < old_cap; i++) {
    if (old_occupied[i] == 0) {
      continue;
    }

    void *element = (char *)old_elems + i * table->stride;

    magpie_ht_insert(table, element);
  }

  free(old_elems);
  free(old_occupied);
}

static void magpie_ht_resize_up(MagpieHashTable *table) {
  magpie_ht_resize(table, table->capacity << 1);
}

static void magpie_ht_resize_down(MagpieHashTable *table) {
  magpie_ht_resize(table, table->capacity >> 1);
}

static void magpie_ht_insert(MagpieHashTable *table, void *item) {
  unsigned int hash = table->hash_func(item);
  unsigned int index = hash & (table->capacity - 1);
  unsigned int org_index = index;

  while (table->occupied[index] &&
         table->cmp_func(item, magpie_ht_index(table, index))) {
    index = (index + 1) % table->capacity;
    if (index == org_index) {
      magpie_ht_resize_up(table);
      magpie_ht_insert(table, item);
      return;
    }
  }

  table->occupied[index] = 1;
  memcpy((char *)table->elements + index * table->stride, item, table->stride);
  table->count++;
}

// Writes result to item
// Returns 0 on success
static int magpie_ht_get(MagpieHashTable *table, void *item) {
  unsigned int hash = table->hash_func(item);
  unsigned int index = hash & (table->capacity - 1);
  unsigned int org_index = index;

  // Linear probe until found
  void *element;
  do {
    element = magpie_ht_index(table, index);

    if (table->occupied[index] && table->cmp_func(item, element) == 0)
      break;

    index = (index + 1) % table->capacity;

    if (index == org_index) {
      return 1;
    }

  } while (1);

  memcpy(item, element, table->stride);

  return 0;
}

// Returns zero if found
// Not used
/* static int magpie_ht_remove(MagpieHashTable *table, const void *item) {

  unsigned int hash = table->hash_func(item);
  unsigned int index = hash & (table->capacity - 1);
  unsigned int org_index = index;

  // Linear probe until found
  void *element;
  do {
    element = magpie_ht_index(table, index);

    if (table->occupied[index] && table->cmp_func(item, element) == 0)
      break;

    index = (index + 1) % table->capacity;

    // Looped back; didn't find
    if (index == org_index) {
      return -1;
    }

  } while (1);

  table->occupied[index] = 0;
  table->count--;

  if (table->count < (table->capacity >> 1)) {
    magpie_ht_resize_down(table);
  }

  return 0;
} */

// Removes item and writes the full data of removed into item
// Returns 0 if found
static int magpie_ht_remove_write(MagpieHashTable *table, void *item) {

  unsigned int hash = table->hash_func(item);
  unsigned int index = hash & (table->capacity - 1);
  unsigned int org_index = index;

  // Linear probe until found
  void *element;
  do {
    element = magpie_ht_index(table, index);

    if (table->occupied[index] && table->cmp_func(item, element) == 0)
      break;

    index = (index + 1) % table->capacity;

    // Looped back; didn't find
    if (index == org_index) {
      return -1;
    }

  } while (1);

  memcpy(item, element, table->stride);

  table->occupied[index] = 0;
  table->count--;

  if (table->count < (table->capacity >> 1)) {
    magpie_ht_resize_down(table);
  }

  return 0;
}

static void magpie_ht_iter(MagpieHashTable *table, void (*iter_func)(void *)) {
  for (size_t i = 0; i < table->capacity; i++) {
    if (table->occupied[i] == 0) {
      continue;
    }

    void *element = magpie_ht_index(table, i);
    iter_func(element);
  }
}

struct MagpieLocation {
  const char *file;
  mp_line_t line;
  size_t alloc_count;
};

// A list of all locations of allocations ordererd by alloc_count
struct MagpieLocation *g_magpie_locations = NULL;
size_t g_magpie_location_len = 0;
size_t g_magpie_location_cap = 0;

static void magpie_location_increase(const char *file, mp_line_t line) {
  for (size_t i = 0; i < g_magpie_location_len; i++) {
    // Found
    struct MagpieLocation *location = g_magpie_locations + i;
    if (location->file == file && location->line == line) {
      location->alloc_count++;

      // Keep order
      if (i > 0 &&
          location->alloc_count > g_magpie_locations[i - 1].alloc_count) {
        struct MagpieLocation tmp = *location;
        *location = g_magpie_locations[i - 1];
        g_magpie_locations[i - 1] = tmp;
      }
      return;
    }
  }

  // Location was not found

  // Resize if array is full
  if (g_magpie_location_len >= g_magpie_location_cap) {
    if (g_magpie_location_cap == 0)
      g_magpie_location_cap = MAGPIE_INITIAL_LOCATION_CAPACITY;
    else
      g_magpie_location_cap *= 2;

    g_magpie_locations =
        realloc(g_magpie_locations,
                g_magpie_location_cap * sizeof(*g_magpie_locations));
  }

  g_magpie_locations[g_magpie_location_len] =
      (struct MagpieLocation){.file = file, .line = line, .alloc_count = 1};

  g_magpie_location_len++;
}

static size_t magpie_location_get_count(const char *file, mp_line_t line) {
  for (size_t i = 0; i < g_magpie_location_len; i++) {
    // Found
    struct MagpieLocation *location = g_magpie_locations + i;
    if (location->file == file && location->line == line) {
      return location->alloc_count;
    }
  }
  // Not found
  return 0;
}

/* Public API implementations */

static size_t g_magpie_total_allocs = 0;
static size_t g_magpie_current_allocs = 0;
static size_t g_magpie_current_size = 0;

MagpieHashTable g_magpie_allocations = {0};

size_t magpie_status() {
  magpie_print(MAGPIE_INFO, "Magpie status:\n");
  if (g_magpie_allocations.count > 0) {
    magpie_print(MAGPIE_INFO, "Remaining:\n");
    magpie_ht_iter(&g_magpie_allocations, magpie_allocation_print);
  }
  magpie_print(MAGPIE_INFO, "Total allocations: %zu\n", g_magpie_total_allocs);
  magpie_print(MAGPIE_INFO, "A total of %zu memory blocks remain to be freed\n",
               g_magpie_current_allocs);
  return g_magpie_current_allocs;
}

// Frees internal resources and terminates magpie
// Does not free tracked blocks
// Returns number of remaining blocks
// Use this in the end of the program
size_t magpie_terminate() {
  if (g_magpie_allocations.capacity > 0) {
    magpie_ht_destroy(&g_magpie_allocations);
  }

  if (g_magpie_location_cap > 0) {
    free(g_magpie_locations);
    g_magpie_location_len = 0;
    g_magpie_location_cap = 0;
  }

  return g_magpie_current_allocs;
}

// Returns the total number of allocations
size_t magpie_total_allocs() { return g_magpie_total_allocs; }

// Returns number of current allocations
size_t magpie_current_allocs() { return g_magpie_current_allocs; }

// Returns sum of total allocations memory (constant time)
size_t magpie_current_size() { return g_magpie_current_size; }

// Validates a tracked pointer
static enum MagpieStatus
magpie_validate_internal(struct MagpieAllocation *alloc) {
  // Check padding if any
  if (alloc->padlen > 0 && memcmp((char *)alloc->ptr + alloc->size,
                                  MAGPIE_PAD_PATTERN, MAGPIE_PADLEN)) {
    return MAGPIE_BUFFEROW;
  }
  return MAGPIE_OK;
}

enum MagpieStatus magpie_validate(void *p) {
  struct MagpieAllocation alloc = (struct MagpieAllocation){.ptr = p};

  if (magpie_ht_get(&g_magpie_allocations, &alloc)) {
    return MAGPIE_UNTRACKED;
  }
  return magpie_validate_internal(&alloc);
}

static void magpie_validate_iter(void *a) {
  (void)magpie_validate_internal((struct MagpieAllocation *)a);
}

void magpie_validate_all() {
  magpie_ht_iter(&g_magpie_allocations, magpie_validate_iter);
}

void magpie_print_locations(long n) {
  size_t off = n == -1 ? 1 : g_magpie_location_len - n + 1;

  for (size_t i = off; i <= g_magpie_location_len; i++) {
    struct MagpieLocation *location =
        g_magpie_locations + g_magpie_location_len - i;

    magpie_print(MAGPIE_INFO, "  %s:%u\t %zu allocations\n", location->file,
                 location->line, location->alloc_count);
  }
}

// Called automatically
static void magpie_ensure_init() {
  // Initialize table
  if (g_magpie_allocations.capacity == 0) {
    g_magpie_allocations = magpie_ht_create(
        sizeof(struct MagpieAllocation), MAGPIE_INITIAL_CAPACITY,
        magpie_allocation_hash, magpie_allocation_cmp);
  }
}

void magpie_track(void *p, size_t size, void *oldptr, size_t oldsize,
                  char padlen, const char *file, mp_line_t line) {

  magpie_ensure_init();
  size_t alloc_num = magpie_location_get_count(file, line);

  struct MagpieAllocation alloc = {
      .ptr = p,
      .oldptr = oldptr,
      .oldsize = oldsize,
      .file = file,
      .line = line,
      .size = size,
      .num = alloc_num,
      .padlen = padlen,
  };

  magpie_ht_insert(&g_magpie_allocations, &alloc);
  magpie_location_increase(file, line);

  g_magpie_current_allocs++;
  g_magpie_current_size += size;
  g_magpie_total_allocs++;
}

// Removes a pointer from magpie tracking
// Validates from buffer overwrite
// Does not free p
static enum MagpieStatus magpie_untrack_internal(void *p, const char *file,
                                                 mp_line_t line,
                                                 struct MagpieAllocation *out) {
  (void)file;
  (void)line;

  magpie_ensure_init();

  struct MagpieAllocation alloc = (struct MagpieAllocation){.ptr = p};

  if (magpie_ht_remove_write(&g_magpie_allocations, &alloc)) {
    return MAGPIE_UNTRACKED;
  }

  g_magpie_current_allocs--;
  g_magpie_current_size -= alloc.size;

  if (out)
    *out = alloc;

  enum MagpieStatus ptr_valid = magpie_validate_internal(&alloc);
  if (ptr_valid != MAGPIE_OK) {
    return ptr_valid;
  }

  return MAGPIE_OK;
}

enum MagpieStatus magpie_untrack(void *p, const char *file, mp_line_t line) {
  enum MagpieStatus status = magpie_untrack_internal(p, file, line, NULL);
  if (status != MAGPIE_OK) {
    magpie_print(
        status,
        "%s:%u: Attempt to untrack untracked or previously freed pointer %p\n",
        file, line, p);
    return status;
  }
  return MAGPIE_OK;
}

void *magpie_malloc(size_t size, const char *file, mp_line_t line) {
  void *p = malloc(size + MAGPIE_PADLEN);
  if (p == NULL) {
    magpie_print(MAGPIE_BAD_ALLOC,
                 "%s:%u: Failed to malloc buffer of size %zu bytes\n", file,
                 line, size);
    return NULL;
  }

  // Fill padding pattern
  memcpy((char *)p + size, MAGPIE_PAD_PATTERN, MAGPIE_PADLEN);

  magpie_track(p, size, NULL, 0, MAGPIE_PADLEN, file, line);

  return p;
}

void *magpie_calloc(size_t num, size_t elemsize, const char *file,
                    mp_line_t line) {
  // No validation padding
  void *p = calloc(num, elemsize);
  if (p == NULL) {
    magpie_print(MAGPIE_BAD_ALLOC,
                 "%s:%u: Failed to calloc buffer of size %zu*%zu bytes\n", file,
                 line, num, elemsize);
    return NULL;
  }

  magpie_track(p, num * elemsize, NULL, 0, 0, file, line);

  return p;
}

void *magpie_realloc(void *p, size_t size, const char *file, mp_line_t line) {
  // New allocation
  if (p == NULL) {
    return magpie_malloc(size, file, line);
  }

  struct MagpieAllocation oldalloc = {.ptr = p};

  enum MagpieStatus status = magpie_untrack_internal(p, file, line, &oldalloc);
  if (status != MAGPIE_OK) {
    magpie_print(
        status,
        "%s:%u: Attempt to realloc untracked or previously freed pointer %p\n",
        file, line, p);
    // Dont return
  }

  void *newp = realloc(p, size + MAGPIE_PADLEN);
  if (newp == NULL) {
    magpie_print(
        MAGPIE_BAD_ALLOC,
        "%s:%u: Failed to reallocate buffer of size %zu -> %zu bytes\n", file,
        line, oldalloc.size, size);
    return NULL;
  }

  // Fill padding pattern
  memcpy((char *)newp + size, MAGPIE_PAD_PATTERN, MAGPIE_PADLEN);

  magpie_track(newp, size, oldalloc.ptr, oldalloc.size, MAGPIE_PADLEN, file,
               line);

  return newp;
}

void magpie_free(void *p, const char *file, mp_line_t line) {
  struct MagpieAllocation alloc = {0};

  enum MagpieStatus status = magpie_untrack_internal(p, file, line, &alloc);

  if (status == MAGPIE_OK) {
  } else if (status == MAGPIE_UNTRACKED) {
    magpie_print(
        status,
        "%s:%u: Attempt to free untracked or previously freed pointer %p\n",
        file, line, p);
    return;
  } else if (status == MAGPIE_BUFFEROW) {
    magpie_print(status,
                 "%s:%u: Buffer overwrite on pointer %p with size %zu "
                 "allocated at %s:%u\n",
                 file, line, alloc.ptr, alloc.size, alloc.file, alloc.line);

    // Don't return, do still free the pointer since it is valid
  }

  free(p);
}

#endif

// Header again

// Tracks an externally allocated pointer
#define MAGPIE_TRACK(p) magpie_track(p, __FILE__, __LINE__)
// Untracks an externally allocated pointer
#define MAGPIE_UNTRACK(p) magpie_untrack(p, __FILE__, __LINE__)

#ifdef MAGPIE_ACTIVATE
#undef malloc
#undef calloc
#undef realloc
#undef free
#define malloc(s) magpie_malloc(s, __FILE__, __LINE__)
#define calloc(n, s) magpie_calloc(n, s, __FILE__, __LINE__)
#define realloc(p, s) magpie_realloc(p, s, __FILE__, __LINE__)
#define free(p) magpie_free(p, __FILE__, __LINE__)
#endif
