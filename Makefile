CC=clang
CFLAGS=-g -Wall -Wextra -pedantic -Wshadow -Wpointer-arith -std=c99

all: tests

%.o: %.c %.h
	$(CC) $(CFLAGS) -c $^

tests: 
	$(MAKE) -C $@

clean:
	$(MAKE) -C tests clean

.PHONY: all tests
