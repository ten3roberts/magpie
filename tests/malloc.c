#include <stdio.h>
// Just because the makefile defines these for clangd
#ifndef MAGPIE_ACTIVATE
#define MAGPIE_ACTIVATE
#endif

// Just because the makefile defines these for clangd
#ifndef MAGPIE_IMPLEMENTATION
#define MAGPIE_IMPLEMENTATION
#endif
#include "magpie.h"

#include <string.h>

void do_work(int lim) {
  char *p;
  for (int i = 0; i < lim; i++) {
    p = malloc(1024);
    strncpy(p, __FILE__, 1024);
  }
  free(p);
}

char *do_work2_ptrs[256] = {0};

void do_work2(unsigned int lim) {
  for (unsigned int i = 0; i < lim; i++) {
    size_t size = rand() % 1024;
    char *ptr = calloc(1, size);
    memset(ptr, i, size);
    do_work2_ptrs[i] = ptr;
  }
}

void do_work2_free() {
  for (unsigned int i = 0; i < sizeof(do_work2_ptrs) / sizeof(*do_work2_ptrs);
       i++) {

    if (do_work2_ptrs[i] != NULL)
      free(do_work2_ptrs[i]);
    do_work2_ptrs[i] = NULL;
  }
}

int main() {
  char *str = malloc(10);
  strncpy(str, "hello, world!", 10);
  puts(str);

  do_work(1);
  do_work(3);
  do_work(1);
  do_work(7);

  do_work2(30);
  do_work2_free();

  do_work2(10);
  do_work2_free();

  malloc(10);

  free(str);

  magpie_status();

  printf("Printing locations: \n");
  magpie_print_locations(-1);
  /* return magpie_terminate(); */

  return 0;
}
