#include <stdio.h>
// Just because the makefile defines these for clangd
#ifndef MAGPIE_ACTIVATE
#define MAGPIE_ACTIVATE
#endif

// Just because the makefile defines these for clangd
#ifndef MAGPIE_IMPLEMENTATION
#define MAGPIE_IMPLEMENTATION
#endif
#include "magpie.h"

#include <string.h>

void test_bufferow() {
  char *str = malloc(10);
  magpie_validate_all();
  strcpy(str, "hello there");
  free(str);
}

int main() {
  test_bufferow();

  magpie_status();

  return magpie_terminate();

  return 0;
}
