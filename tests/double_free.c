#include <stdio.h>
// Just because the makefile defines these for clangd
#ifndef MAGPIE_ACTIVATE
#define MAGPIE_ACTIVATE
#endif

// Just because the makefile defines these for clangd
#ifndef MAGPIE_IMPLEMENTATION
#define MAGPIE_IMPLEMENTATION
#endif
#include "magpie.h"

#include <string.h>

int main() {
  char* str = malloc(10);

  free(str);
  str = NULL; // If str isn't set to NULL, str == leak and free(str) on line 22 frees leak.

  char* leak = malloc(10);
  strncpy(leak, "hello", 10);
  free(str);

  puts(leak);

  free(leak);
  magpie_status();

  return magpie_terminate();

  return 0;
}
