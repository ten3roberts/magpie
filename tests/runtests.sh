#!/bin/sh

echoerr() { echo "\033[31;1m$@\033[0m" 1>&2; }

cd `dirname $0`

make > /dev/null || exit 1
if [ -n "$1" ]; then
  tests="./bin/$1"
else
  tests="./bin/*"
fi

for test in $tests; do
  echo "\033[033;1mRunning:\033[0m $test"
  ./$test || { errcode=$?; echoerr "Test $test failed"; exit $errcode; }
done

echo "\033[032;1mAll tests succeeded!\033[0m"
