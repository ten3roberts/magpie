#include <stdio.h>
// Just because the makefile defines these for clangd
#ifndef MAGPIE_ACTIVATE
#define MAGPIE_ACTIVATE
#endif

// Just because the makefile defines these for clangd
#ifndef MAGPIE_IMPLEMENTATION
#define MAGPIE_IMPLEMENTATION
#endif
#include "magpie.h"

#include <string.h>

int main() {
  /* for (int i = 0; i < 1024; i++) { */
  /*   calloc(2, 100); */
  /* } */

  size_t len = 16;
  char *str = NULL;
  for (char c = 'a'; c <= 'd'; c++) {
    str = realloc(str, len + 1);
    memset(str, c, len);
    str[len] = '\0';
    puts(str);
    len *= 2;
  }

  free(str);

  magpie_status();

  return magpie_terminate();

  return 0;
}
