#include <stdio.h>
// Just because the makefile defines these for clangd
#ifndef MAGPIE_ACTIVATE
#define MAGPIE_ACTIVATE
#endif

// Just because the makefile defines these for clangd
#ifndef MAGPIE_IMPLEMENTATION
#define MAGPIE_IMPLEMENTATION
#endif
#include "magpie.h"

#include <string.h>

int main() {
  char* str = "I am static";
  free(str);

  magpie_status();

  return magpie_terminate();

  return 0;
}
