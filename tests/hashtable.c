#include <stdio.h>
// Just because the makefile defines these for clangd
#ifndef MAGPIE_ACTIVATE
#define MAGPIE_ACTIVATE
#endif

// Just because the makefile defines these for clangd
#ifndef MAGPIE_IMPLEMENTATION
#define MAGPIE_IMPLEMENTATION
#endif
#include "magpie.h"

#include <assert.h>
#include <string.h>

int main() {
  MagpieHashTable table =
      magpie_ht_create(sizeof(struct MagpieAllocation), 4,
                       magpie_allocation_hash, magpie_allocation_cmp);

  void *ptrs[10];
  for (unsigned int i = 0; i < sizeof(ptrs) / sizeof(*ptrs); i++) {
    ptrs[i] = malloc(1024);
    magpie_ht_insert(&table,
                     &(struct MagpieAllocation){.ptr = ptrs[i], .line = i});
  }

  for (unsigned int i = 0; i < sizeof(ptrs) / sizeof(*ptrs); i++) {
    struct MagpieAllocation alloc = {.ptr = ptrs[i]};
    assert(magpie_ht_get(&table, &alloc) == 0);

    assert(alloc.line == i);
    magpie_allocation_print(&alloc);
  }

  for (unsigned int i = 0; i < 8; i++) {
    struct MagpieAllocation alloc = {.ptr = ptrs[i]};
    magpie_ht_remove_write(&table, &alloc);
  }

  for (unsigned int i = 0; i < sizeof(ptrs) / sizeof(*ptrs); i++) {
    struct MagpieAllocation alloc = {.ptr = ptrs[i]};

    magpie_allocation_print(&alloc);
  }

  for (unsigned int i = 0; i < 4; i++) {
    struct MagpieAllocation alloc = {.ptr = ptrs[i]};
    assert(magpie_ht_get(&table, &alloc) != 0);
  }

  for (unsigned int i = 0; i < sizeof(ptrs) / sizeof(*ptrs); i++) {
    free(ptrs[i]);
  }
  magpie_ht_destroy(&table);

  magpie_status();

  return magpie_terminate();
  return 0;
}
