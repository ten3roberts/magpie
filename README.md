# Magpie
Magpie is a drop-in single header memory leak detection library.

The aim is to track allocations and where they are freed, print unfreed blocks,
detect double frees and detect buffer overwrites

magpie is **NOT** a debugger that runs separetely from your program but rather a
library that tracks the programs allocations. It thus has no performance
penalties except for malloc, calloc, realloc and free when `magpie_activate` is
defined. This means you can add the single header to your program and let it
detect leaks in debug mode but disable itself in release.

## What it does
* tracks allocations and deallocations
* stores file and line of allocations
* reports unfreed blocks
* tracks size of pointer
* tracks allocation index of allocations from the same place
* summary of leaked pointers including: address, size, file, line, index from
  same place (mp_status)
* Detect buffer overwrites on free
* Live pointer validation with magpie_validate
* Validate all pointers with magpie_validate_all (slow)
* statistics over total allocations
* statistics over number of allocations per location (magpie_print_locations)

## What it doesn't do
* track pointer usage
* record backtraces

## Usage
### Setup
copy [magpie.h](magpie.h) into your project and define `magpie_implementation`
above the include
in one .c source file to generate the implementation.

```
#include ...
#include ...
#define magpie_implementation
#include "magpie.h"
```

Define `MAGPIE_ACTIVATE` as a compile definition or above every include to replace default malloc and
free with magpie's wrapper that tracks the allocation.

This builds magpie into your project rather than linking it as a static library.
This means magpie follows your projects compile definitions. As such, magpie is
developed to not generate any warnings with -Wall -Wextra and -pedantic.

Make sure to include "magpie.h" instead of <stdlib.h> to always override the
allocators.

External pointers from other libraries can be tracked and untracked (malloced
and freed) with MAGPIE_TRACK and MAGPIE_UNTRACK respectively

### Reporting leaked pointers
Put magpie_status() at the end to print all remaining allocations

return magpie_terminate() instead of 0 to fail if any memory remains to free

Put magpie_print_locations() to get an overview of where allocations happen

```
int main(int argc, char **argv) {
  ...

  mp_status(); // Prints remaining blocks to set output stream
  return mp_terminate(); // Frees internal resources and return non-zero on
  remaining allocations
}
```

## Known limitations
Malloc is smart and tends to re-use pointers when allocated one after another of
similar size

This can hide some immediate doubles frees when two allocations are aliased when
allocated and freed back to back.

Although, a double free is still detected when p2 is freed. If p2 was not freed
in the end
the leak would not occur since p2 was freed on the second invocation of
free(p1). This is still unintended behaviour and it is thus recommended to
assign NULL to pointers after freeing to avoid this simple behaviour.

See [./tests/double_free.c](./tests/double_free.c) for more.

```
char *p1 = malloc(16);
free(p1);
char *p2 = malloc(16);
// p1 == p2 since malloc reuses the memory due to size and proximity in time
free(p1);

// p2 is not valid here

// Double free is detected here
free(p2);
```

## License
Magpie is licensed under the MIT license. This license is included both as a
separate file and in `magpie.h` to allow for only including the header in your
project without keeping a separate license file.
